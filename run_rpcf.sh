#!/bin/bash

echo ">> Dropping caches"
sudo sh -c 'echo 3 >/proc/sys/vm/drop_caches'

echo ">> Running program"
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )" # allow running from other dirs
$SCRIPTPATH/rpcf

