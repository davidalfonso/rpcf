#include <cstdio>
#include <cstring>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

constexpr unsigned int MB = 1024*1024;
constexpr unsigned int GB = 1024*MB;
constexpr unsigned int DATA_BLOCK_SIZE = MB;

// [todo] - review file size to make it POSIX conformant
constexpr unsigned int FILE_SIZE = GB;
constexpr int NUM_CYCLES = 2;
constexpr int NUM_FILES = 4;

using namespace std;

/*
 * Handles a file. Lacks error checking, but it's OK given the purpose of the
 * this program.
 * [todo] - add timing in each operation
 */
class File
{
public:
    File(int id, unsigned int size)
    : id_(id), size_(size) 
    { }
    void create_empty() {
        FILE* f = fopen(name().c_str(), "w");
        fclose(f);
    }
    void write_in_blocks(char* buffer, unsigned int buffer_size) {
        cout << "writing " << name() << " (" << size_ << " bytes)" << endl;
        FILE* f = fopen(name().c_str(), "rb+");
        // turn off internal buffering
        setbuf(f, nullptr); 
        while (ftell(f) < size_) {
            fwrite(buffer, buffer_size, 1, f);
        }
        fclose(f);
    }
    void remove() {
        std::remove(name().c_str());
    }
private:
    string name() const { 
        stringstream ss;
        ss << "rpcf." << id_;
        return ss.str(); 
    }
private:
    int id_;
    unsigned int size_;
};

/*
 * This program simulates writes in 1MB blocks to a number of files in a circular
 * manner, i.e. in FIFO order.
 */
int main(int argc, char* argv[])
{
    vector<File> files;
    for (int id = 0; id < NUM_FILES; ++id)
        files.push_back(File(id, FILE_SIZE));

    // Create all files empty
    for (auto& f : files)
        f.create_empty();

    // Write all files a number of cycles
    char* buffer = new char[DATA_BLOCK_SIZE];
    memset(buffer, 0, DATA_BLOCK_SIZE);
    for (int i = 0; i < NUM_CYCLES; ++i) {
        for (auto& f : files)
            f.write_in_blocks(buffer, DATA_BLOCK_SIZE);
    }
    delete [] buffer;

    // Delete all files
    for (auto& f : files)
        f.remove();

    return 0;
}

